
Pod::Spec.new do |spec|

  spec.name         = "flutterPodd"
  spec.version      = "1.0.6"
  spec.summary      = "Example Framework"
  spec.description  = "Description for exmple framework."
  spec.homepage     = "https://gitlab.com/Deepakkanna/flutterpod"
  spec.license      = { :type => 'Apache-2.0', :file => 'LICENSE' }
  spec.author       = { "Deepakkanna" => "deepakk@linarc.io" }
  spec.platform     = :ios, "14.0"
  spec.source       = { :git => "https://gitlab.com/Deepakkanna/flutterpod.git", :tag => spec.version.to_s }
  spec.source_files  = "flutterPod/**/*.{swift}"
  spec.swift_versions = "5.5"
end
